import lojaintegrada as li
import oracle as ora
import schedule
import time

def get_order():
    print('inicio')
    data_all_order = li.get_all_orders(url='api/v1/pedido/search')

    if (len(data_all_order) > 0):
        for data_order in data_all_order:
            data_order_details = li.get_order_details(url=data_order['resource_uri'])
            data_costumer = li.get_order_customer(url=data_order['cliente_uri'])
            data_item = li.get_order_item(url=data_order['resource_uri'])
            
            ora.inset_order(data_order_details)
            ora.inset_costumer(data_costumer)
            ora.inset_order_item(data_item) 
        print('fim')
    else:
        print('Sem pedidos novos')

schedule.every(5).minutes.do(get_order)

while True:
    schedule.run_pending()
    time.sleep(1)


  





