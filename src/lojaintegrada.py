import requests
from urllib.parse import urljoin
from datetime import datetime
import os

base_url = "https://api.awsli.com.br"


params = {
    'format':'json',
    'chave_api': os.getenv('LI_API_KEY'),
    'chave_aplicacao':os.getenv('LI_APPLICATION_KEY')
}


def get_all_orders(url):

    params['limit'] = 50
    params['since_criado'] = datetime.today().date()

    url = urljoin(base=base_url,url=url)
    orders = []

    try:
        req = requests.get(url=url,params=params)
        if req.status_code == 200:
            req_json = req.json()
            req_json = req_json['objects']
            for i in range(0,len(req_json),1):
                if (req_json[i]['situacao']['codigo']=='pedido_pago'):
                    orders.append(
                            {
                                'pedido_id':req_json[i]['numero'],
                                'resource_uri':req_json[i]['resource_uri'],
                                'cliente_uri':req_json[i]['cliente']
                            }
                        )
            return orders
    except:
        print(f'HTTP error occurred')

def get_order_details(url):

    url = urljoin(base=base_url,url=url)

    orders_details = []

    try:
        req = requests.get(url=url,params=params)
        
        if req.status_code == 200:
            req_json = req.json()
            orders_details.append(
                    {
                        'URL':req_json['resource_uri'],
                        'CLIENTEURL':req_json['cliente']['resource_uri'],
                        'NROCLIENTE':req_json['cliente']['id'],
                        'OBS':req_json['cliente_obs'],
                        'DATA':req_json['data_criacao'],
                        'BAIRRO':req_json['endereco_entrega']['bairro'],
                        'CEP':req_json['endereco_entrega']['cep'],
                        'CIDADE':req_json['endereco_entrega']['cidade'],
                        'COMPLEMENTO':req_json['endereco_entrega']['complemento'],
                        'ENDERECO':req_json['endereco_entrega']['endereco'],
                        'UF':req_json['endereco_entrega']['estado'],
                        'NUMERO':req_json['endereco_entrega']['numero'],
                        'ENVIONOME':req_json['envios'][0]['forma_envio']['nome'],
                        'ENVIOPRAZO':req_json['envios'][0]['prazo'],
                        'NROPEDIDO':req_json['numero'],
                        'AUTHCODE':req_json['pagamentos'][0]['authorization_code'],
                        'BANDEIRA':req_json['pagamentos'][0]['bandeira'],
                        'PAGTOIMAGEM':req_json['pagamentos'][0]['forma_pagamento']['imagem'],
                        'PAGTOTIPO':req_json['pagamentos'][0]['pagamento_tipo'],
                        'PAGTONROPARCELA':req_json['pagamentos'][0]['parcelamento']['numero_parcelas'],
                        'PAGTOVLRPARCELA':req_json['pagamentos'][0]['parcelamento']['valor_parcela'],
                        'TRANSID':req_json['pagamentos'][0]['transacao_id'],
                        'PAGTOVALOR':req_json['pagamentos'][0]['valor'],
                        'PAGTOVALORPAGO':req_json['pagamentos'][0]['valor_pago'],
                        'APROVADO':req_json['situacao']['aprovado'],
                        'CANCELADO':req_json['situacao']['cancelado'],
                        'DESCONTO':req_json['valor_desconto'],
                        'VLRENVIO':req_json['valor_envio'],
                        'SUBTOTAL':req_json['valor_subtotal'],
                        'TOTAL':req_json['valor_total']
                    }
                )
            return orders_details
    except :
        print(f'HTTP error occurred')

def get_order_item(url):

    url = urljoin(base=base_url,url=url)

    orders_itens = []

    try:
        req = requests.get(url=url,params=params)
        
        if req.status_code == 200:
            req_json = req.json()
            req_json_items = req_json['itens']
            for i in range(0,len(req_json_items),1):
                req_item = requests.get(url=urljoin(base_url,req_json_items[i]['produto']),params=params)
                req_item = req_item.json()
                
                orders_itens.append({
                        'nropedido':req_json['numero'],
                        'ncm':req_json_items[i]['ncm'],
                        'descricao':req_json_items[i]['nome'],
                        'precocheio':req_json_items[i]['preco_cheio'],
                        'custo':req_json_items[i]['preco_custo'],
                        'precopromo':req_json_items[i]['preco_promocional'],
                        'subtotal':req_json_items[i]['preco_subtotal'],
                        'precovenda':req_json_items[i]['preco_venda'],
                        'quantidade':req_json_items[i]['quantidade'],
                        'sku':req_json_items[i]['sku'],
                        'ean':req_item['gtin'],
                        'nroproduto':req_item['id']
                    })
                                    
            return orders_itens
    except :
        print(f'HTTP error occurred')

def get_order_customer(url):

    url = urljoin(base=base_url,url=url)

    orders_costumer = []

    try:
        req = requests.get(url=url,params=params)
        
        if req.status_code == 200:
            req_json = req.json()

            orders_costumer.append({
                    'CNPJ':req_json['cnpj'],
                    'CPF':req_json['cpf'],
                    'DTANASCIMENTO':req_json['data_nascimento'],
                    'EMAIL':req_json['email'],
                    'NROCLIENTE':req_json['id'],
                    'NOME':req_json['nome'],
                    'NOMERAZAO':req_json['razao_social'],
                    'RG':req_json['rg'],
                    'SEXO':req_json['sexo'],
                    'CELULAR':req_json['telefone_celular'],
                    'TELCOMERCIAL':req_json['telefone_comercial'],
                    'TELPRINCIPAL':req_json['telefone_principal'],
                    'TIPO':req_json['tipo'],
                    'IE':req_json['ie']
                })
                                    
            return orders_costumer
    except :
        print(f'HTTP error occurred')





