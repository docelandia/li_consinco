import cx_Oracle
import os
import queries
schema = 'apex'
strig_connection= '10.10.254.20:1521/orcl'
pwd = os.getenv('ORA_APEX_PASS')

def connectDB():
    try:
        conn = cx_Oracle.connect(schema,pwd,strig_connection,encoding="UTF-8")
        return conn
    except:
        print("falha na conexao com o banco")


def inset_order(data):

    try:
        oracle = connectDB()
        cursor = oracle.cursor()
        for order in data:
            print(order)
            cursor.execute(
                queries.query_insert_order,
                order
            )
        oracle.commit()
        oracle.close()
    except cx_Oracle.DatabaseError as exc:
        error, = exc.args
        print("Oracle-Error-Code:", error.code)
        print("Oracle-Error-Message:", error.message)


def inset_costumer(data):

    try:
        oracle = connectDB()
        cursor = oracle.cursor()
        for costumer in data:
            cursor.execute(
                queries.query_insert_order_costumer,
                costumer
            )
        oracle.commit()
        oracle.close()
    except cx_Oracle.DatabaseError as exc:
        error, = exc.args
        print("Oracle-Error-Code:", error.code)
        print("Oracle-Error-Message:", error.message)


def inset_order_item(data):

    try:
        oracle = connectDB()
        cursor = oracle.cursor()
        for item in data:
            cursor.execute(
                queries.query_insert_order_item,
                item
            )
        oracle.commit()
        oracle.close()
    except cx_Oracle.DatabaseError as exc:
        error, = exc.args
        print("Oracle-Error-Code:", error.code)
        print("Oracle-Error-Message:", error.message)

